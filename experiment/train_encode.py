import os
import argparse

from ckn.data.loader import EncodeLoader
from ckn.models import unsupCKN, supCKN
from ckn.scores import compute_metrics
from torch.utils.data import DataLoader
import torch
from torch import nn
from torch.optim.lr_scheduler import StepLR, MultiStepLR, ReduceLROnPlateau
import torch.optim as optim
import numpy as np

from timeit import default_timer as timer

name = 'encode'
datadir = '../data/{}'.format(name)


def load_args():
    parser = argparse.ArgumentParser(
        description="CKN-seq for Encode experiments")
    parser.add_argument(
        '--seed', type=int, default=1, metavar='S',
        help='random seed (default: 1)')
    parser.add_argument(
        "--tfid", metavar="tfid", dest="tfid", default=[], nargs='*',
        type=int, help="tfids to generate experiments")
    parser.add_argument(
        '--batch-size', type=int, default=128, metavar='M',
        help='input batch size for training (default: 128)')
    parser.add_argument(
        '--epochs', type=int, default=100, metavar='N',
        help='number of epochs to train (default: 100)')
    parser.add_argument(
        "--num-motifs", dest="n_motifs", metavar="m",
        default=[128], nargs='+', type=int,
        help="number of filters for each layer (default [128])")
    parser.add_argument(
        "--len-motifs", dest="len_motifs", metavar="l",
        default=[12], nargs='+', type=int,
        help="filter size for each layer (default: [12])")
    parser.add_argument(
        "--subsamplings", dest="subsamplings", metavar="s",
        default=[1], nargs='+', type=int,
        help="subsampling for each layer (default: [1])")
    parser.add_argument(
        "--sigma", dest="kernel_params", default=[0.3],
        nargs='+', type=float, help="sigma for each layer (default: [0.3])")
    parser.add_argument(
        "--rc", action='store_false',
        help="use reverse complement mode or not (default: true)")
    parser.add_argument(
        "--sampling-patches", dest="n_sampling_patches", default=250000,
        type=int, help="number of sampled patches (default: 250000)")
    parser.add_argument(
        "--kfold", dest="kfold", default=5, type=int,
        help="k-fold cross validation (default: 5)")
    parser.add_argument(
        "--ntrials", dest="ntrials", default=1, type=int,
        help="number of trials for training (default: 1)")
    parser.add_argument(
        "--penalty", metavar="penal", dest="penalty", default='l2',
        type=str, choices=['l2', 'l1'],
        help="regularization used in the last layer (default: l2)")
    parser.add_argument(
        "--outdir", metavar="outdir", dest="outdir",
        default='', type=str, help="output path(default: '')")
    parser.add_argument(
        "--method", type=str, default='unsup',
        choices=['unsup', 'sup', 'hybrid'],
        help="training method (default: unsup)")
    parser.add_argument(
        "--regularization", type=float, default=1e-6,
        help="regularization parameter for sup CKN")
    parser.add_argument(
        "--preprocessor", type=str, default='standard_row',
        choices=['standard_row', 'standard_col'],
        help="preprocessor for unsup CKN (default: standard_row)")
    parser.add_argument(
        "--use-cuda", action='store_true', default=False,
        help="use gpu (default: False)")
    parser.add_argument(
        "--pooling", default='mean', choices=['mean', 'max'], type=str,
        help='mean or max global pooling (default: mean)')
    parser.add_argument(
        "--noise", type=float, default=0.0, help="perturbation percent")
    # parser.add_argument("--lr", type=float, default=0.1, help='learning rate')
    parser.add_argument(
        "--unsup-dir", type=str, default=None,
        help="unsup model path for hybrid model")
    parser.add_argument(
        "--logo", action='store_true',
        help="generate logo, only useful after setting outdir (default: False)")
    args = parser.parse_args()
    args.use_cuda = args.use_cuda and torch.cuda.is_available()
    # check shape
    assert len(args.n_motifs) == len(args.len_motifs) == len(args.subsamplings) == len(args.kernel_params), "numbers mismatched"
    args.n_layers = len(args.n_motifs)
    torch.manual_seed(args.seed)
    if args.use_cuda:
        torch.cuda.manual_seed(args.seed)
    np.random.seed(args.seed)

    args.save_logs = False
    if args.outdir != "":
        args.save_logs = True
        outdir = args.outdir
        if not os.path.exists(outdir):
            try:
                os.makedirs(outdir)
            except:
                pass
        outdir = outdir + "/{}".format(name)
        if not os.path.exists(outdir):
            try:
                os.makedirs(outdir)
            except:
                pass
        outdir = outdir + "/{}".format(args.method)
        if not os.path.exists(outdir):
            try:
                os.makedirs(outdir)
            except:
                pass
        outdir = outdir + "/noise_{}".format(args.noise)
        if not os.path.exists(outdir):
            try:
                os.makedirs(outdir)
            except:
                pass
        outdir = outdir + "/{}".format(args.pooling)
        if not os.path.exists(outdir):
            try:
                os.makedirs(outdir)
            except:
                pass
        outdir = outdir+'/{}_{}_{}_{}'.format(
            args.n_layers, args.len_motifs, args.n_motifs, args.subsamplings)
        if not os.path.exists(outdir):
            try:
                os.makedirs(outdir)
            except:
                pass
        outdir = outdir+'/{}'.format(args.kernel_params)
        if not os.path.exists(outdir):
            try:
                os.makedirs(outdir)
            except:
                pass
        args.outdir = outdir

    return args


def load_model(path, M=unsupCKN):
    check_point = torch.load(path)
    args = check_point['args']
    model = M(
        4, args.n_motifs, args.len_motifs, args.subsamplings,
        kernel_args_list=args.kernel_params, alpha=args.regularization,
        reverse_complement=args.rc, fit_bias=args.fit_bias,
        global_pool=args.pooling, penalty=args.penalty, scaler=args.preprocessor)
    model.load_state_dict(check_point['state_dict'])
    model.train(False)
    return model


def main():
    args = load_args()
    print(args)
    datasets = EncodeLoader(datadir, pre_padding=args.len_motifs[0] - 1)
    tfids = datasets.get_ids(args.tfid)
    for tfid in tfids:
        torch.manual_seed(args.seed)
        if args.use_cuda:
            torch.cuda.manual_seed(args.seed)
        np.random.seed(args.seed)
        print("tfid: {}".format(tfid))
        if args.save_logs:
            tfid_outdir = args.outdir + '/{}'.format(tfid)
            if not os.path.exists(tfid_outdir):
                os.makedirs(tfid_outdir)
        loader_args = {}
        if args.use_cuda:
            loader_args = {'num_workers': 1, 'pin_memory': True}

        pos_dset = datasets.get_tensor(tfid, val_split=0, generate_neg=False)
        pos_loader = DataLoader(
            pos_dset, batch_size=args.batch_size, shuffle=False, **loader_args)

        # set regularization parameter for small-scale dataset
        n_train = 2 * len(pos_dset)
        if n_train < 1.5e+4:
            args.regularization = min(.5/n_train, 5e-4)
            if args.method == "hybrid":
                args.regularization *= 0.2
        print(args)

        if args.method == "unsup":
            M = unsupCKN
            args.fit_bias = False
            if args.preprocessor == 'standard_col':
                search_grid = 2. ** np.arange(2, -6, -1)
            else:
                search_grid = 2. ** np.arange(11, 3, -1)
            search_grid = search_grid.tolist()
            train_dset = datasets.get_tensor(tfid, val_split=0, fixed_noise=args.noise)
            # train_dset.augment(noise=args.noise)
            train_loader = DataLoader(
                train_dset, batch_size=args.batch_size, shuffle=False, **loader_args)
        else:
            M = supCKN
            args.fit_bias = True
            train_dset, val_dset = datasets.get_tensor(
                tfid, val_split=0.25, noise=args.noise)
            train_loader = DataLoader(
                train_dset, batch_size=args.batch_size, shuffle=True, **loader_args)
            val_loader = DataLoader(
                val_dset, batch_size=args.batch_size, shuffle=False, **loader_args)

        if args.n_layers > 1:
            kernel_funcs = ['exp'] + ['add_exp'] * (args.n_layers - 1)
        else:
            kernel_funcs = None

        model = M(
            4, args.n_motifs, args.len_motifs, args.subsamplings,
            kernel_funcs=kernel_funcs,
            kernel_args_list=args.kernel_params, alpha=args.regularization,
            reverse_complement=args.rc, fit_bias=args.fit_bias,
            global_pool=args.pooling, penalty=args.penalty, scaler=args.preprocessor)
        print(model)
        print(len(train_dset))

        tic = timer()
        if args.method == "unsup":
            model.unsup_cross_val(
                train_loader, pos_loader,
                n_sampling_patches=args.n_sampling_patches,
                alpha_grid=search_grid, kfold=args.kfold,
                use_cuda=args.use_cuda)
        else:
            criterion = nn.BCEWithLogitsLoss()
            optimizer = optim.Adam(model.ckn_model.parameters(), lr=0.1)
            patience = 4
            if args.noise > 0:
                patience = 6
            lr_scheduler = ReduceLROnPlateau(
                optimizer, factor=0.5, patience=patience, min_lr=1e-4)
            if args.method == "sup":
                model.sup_train(
                    train_loader, criterion, optimizer, lr_scheduler,
                    init_train_loader=pos_loader, epochs=args.epochs,
                    val_loader=val_loader,
                    n_sampling_patches=args.n_sampling_patches,
                    use_cuda=args.use_cuda)
            elif args.method == "hybrid":
                unsup_model = load_model(args.unsup_dir + "/model.pkl")
                if args.use_cuda:
                    unsup_model.cuda()
                model.hybrid_train(
                    unsup_model, train_loader, criterion,
                    optimizer, lr_scheduler,
                    init_train_loader=pos_loader, epochs=args.epochs,
                    val_loader=val_loader,
                    n_sampling_patches=args.n_sampling_patches,
                    use_cuda=args.use_cuda, regul=1.0)
        toc = timer()
        training_time = (toc - tic) / 60

        print("Testing...")
        test_dset = datasets.get_tensor(tfid, split='test')
        test_loader = DataLoader(
            test_dset, batch_size=args.batch_size, shuffle=False, **loader_args)
        y_pred, y_true = model.predict(test_loader, proba=True, use_cuda=args.use_cuda)
        scores = compute_metrics(y_true, y_pred)
        scores.loc['training_time'] = training_time
        print(scores)
        if args.save_logs:
            scores.to_csv(tfid_outdir + "/metric.csv",
                          header=['value'], index_label='metric')
            np.save(tfid_outdir + "/predict", y_pred.numpy())
            torch.save(
                {'args': args,
                 'state_dict': model.state_dict()},
                tfid_outdir + '/model.pkl')

            if args.logo:
                import matplotlib
                matplotlib.use('Agg')
                from ckn.data.pltlogo import draw_logo, pwm_to_bits
                import matplotlib.pyplot as plt
                model.cpu()
                print("Generating logo...")
                logo_outdir = tfid_outdir + "/logo"
                if not os.path.exists(logo_outdir):
                    os.makedirs(logo_outdir)
                pwm_all = model.compute_motif()
                np.savez_compressed(logo_outdir+'/pwm', pwm=pwm_all)
                for i in range(pwm_all.shape[0]):
                    pwm = pwm_all[i]
                    bits = pwm_to_bits(pwm, trim=True)
                    draw_logo(bits, width_scale=0.8, palette='deepbind')
                    plt.savefig(logo_outdir + "/logo_{}.png".format(i))
                    bits_rc = np.flip(np.flip(bits, axis=0), axis=1)
                    draw_logo(bits_rc, width_scale=0.8, palette='deepbind')
                    plt.savefig(logo_outdir + "/logo_{}_rc.png".format(i))
                    plt.close("all")
        return


def evaluate():
    args = load_args()
    print(args)
    datasets = EncodeLoader(datadir, pre_padding=args.len_motifs[0] - 1)
    tfids = datasets.get_ids(args.tfid)
    for tfid in tfids:
        print("tfid: {}".format(tfid))
        if args.save_logs:
            tfid_outdir = args.outdir + '/{}'.format(tfid)
            if not os.path.exists(tfid_outdir):
                os.makedirs(tfid_outdir)
        loader_args = {}
        if args.use_cuda:
            loader_args = {'num_workers': 1, 'pin_memory': True}
        if args.method == 'unsup':
            M = unsupCKN
        else:
            M = supCKN
        model = load_model(tfid_outdir + '/model.pkl', M=M)
        print(model)

        print("Testing...")
        test_dset = datasets.get_tensor(tfid, split='test')
        test_loader = DataLoader(
            test_dset, batch_size=args.batch_size, shuffle=False, **loader_args)
        y_pred, y_true = model.predict(test_loader, proba=True, use_cuda=args.use_cuda)
        scores = compute_metrics(y_true, y_pred)
        print(scores)

        if args.save_logs:
            scores.to_csv(tfid_outdir + "/metric2.csv",
                          header=['value'], index_label='metric')
            np.save(tfid_outdir + "/predict", y_pred.numpy())


if __name__ == "__main__":
    main()
    # evaluate()
