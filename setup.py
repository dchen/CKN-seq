from setuptools import setup, find_packages, Extension
import os

setup(
    name='CKN-seq',
    version='0.2.0',
    description="Convolutional kernel network for biological sequences",
    author="Dexiong Chen",
    setup_requires=['numpy'],
    install_requires=[
        "numpy",
        "pandas",
        "scikit-learn",
        "scipy",
        "biopython",
    ],
    packages=find_packages()
)