import torch
from torch.autograd import Variable
import numpy as np
from scipy import optimize
from scipy.stats import chi2
from ckn.utils import proj_on_simplex


def sequences_to_pwm(X):
    """Compute the PFM for the given sequences
    X: m x n ndarray, m sequences of length n, the value is 
    between 0 and k (k=|alphabet|, k=4 for DNA).
    """
    pfm = np.zeros((4, X.shape[1]))
    for i in range(X.shape[1]):
        values, counts = np.unique(X[:, i], return_counts=True)
        for j, v in enumerate(values):
            if v > 0:
                pfm[v - 1, i] = counts[j]
    pwm = pfm/np.sum(pfm, axis=0)
    return pwm.T


def compute_pwm(model, max_iter=2000):
    seq_model = model[0]
    embed_layer = seq_model.embed_layer
    ckn_model = seq_model.ckn_model
    n_layers = ckn_model.n_layers 
    n_motifs = ckn_model.out_channels
    weights = model[-1].weight.data[0].clone().numpy()
    abs_weights = np.abs(weights)
    max_weight = np.max(abs_weights)

    filter_indices = np.argsort(abs_weights)[::-1]
    print(weights[filter_indices])
    n_motifs = len(filter_indices)

    pwm_all = []
    ckn_model.train(True)
    print(ckn_model.len_motif)

    alpha = ckn_model[-1].kernel_args[0].data[0]
    l = ckn_model[-1].filter_size
    n = float(ckn_model[-1].out_channels)
    alpha = ckn_model[-1].kernel_args[0].data[0]
    sigma2 = 1./(alpha)
    threshold = 1. - chi2.ppf(0.9, n - 1) * sigma2 / 2
    print("threshold: {}".format(threshold))

    for i, motif_idx in enumerate(filter_indices):
        if abs_weights[motif_idx] <= max_weight * 0.05:
            continue
        motif, loss = optimize_motif(motif_idx, ckn_model, max_iter)
        motif_norm = np.linalg.norm(motif)
        threshold = (1 - motif_norm * np.exp(-4.5)) ** 2
        # print(loss)
        # print("threshold: {}".format(threshold))
        if loss < threshold:
            print("{} is ok".format(i))
            pwm_all.append(motif)
    pwm_all = np.asarray(pwm_all)
    return pwm_all


def optimize_motif(motif_idx, ckn_model, max_iter):
    n_layers = ckn_model.n_layers
    if n_layers == 1:
        motif = ckn_model[-1].weight.data[motif_idx].clone()
        proj_on_simplex(motif)
    else:
        motif = torch.ones(4, ckn_model.len_motif)/4.
    motif = Variable(motif, requires_grad=True)
    filter_c = ckn_model[-1].weight.data[motif_idx:motif_idx+1].clone()
    # filter_c = Variable(filter_c, volatile=True)
    with torch.no_grad():
        filter_c = ckn_model[-1]._conv_layer(filter_c)
        lintrans = ckn_model[-1]._compute_lintrans()
        filter_c = ckn_model[-1]._mult_layer(filter_c, lintrans)
    filter_c = Variable(filter_c.data, requires_grad=False)

    def eval_loss(w):
        w = w.reshape(4, -1)
        if motif.grad is not None:
            motif.grad = None
        motif.data.copy_(torch.from_numpy(w))
        proj_on_simplex(motif.data)
        out1 = ckn_model.representation(motif.unsqueeze(0), n_layers-1)
        out1 = ckn_model[-1]._conv_layer(out1)
        lintrans = ckn_model[-1]._compute_lintrans()
        out2 = ckn_model[-1]._mult_layer(out1, lintrans)
        loss = torch.norm(out2 - filter_c)**2
        loss.backward()
        return loss.item()

    def eval_grad(w):
        dw = motif.grad.data
        return dw.cpu().numpy().ravel().astype('float64')

    w_init = motif.data.cpu().numpy().ravel().astype("float64")

    w = optimize.fmin_l_bfgs_b(
        eval_loss, w_init, fprime=eval_grad,
        maxiter=max_iter, pgtol=1e-8, m=10, disp=0)
    if isinstance(w, tuple):
        w = w[0]
    loss = eval_loss(w)
    return motif.data.numpy().T, loss
