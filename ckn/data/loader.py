import os
import sys
import numpy as np
import pandas as pd
import torch
from Bio import SeqIO
from collections import defaultdict
from sklearn.model_selection import train_test_split

from .data_helper import pad_sequences, TensorDataset, augment
from .dishuffle import doublet_shuffle

if sys.version_info < (3,):
    import string
    maketrans = string.maketrans
else:
    maketrans = str.maketrans

ALPHABETS = {
    'DNA': (
        'ACGT',
        '\x01\x02\x03\x04'
    ),
    'PROTEIN': (
        'ARNDCQEGHILKMFPSTWYV',
        '\x01\x02\x03\x04\x05\x06\x07\x08\t\n\x0b\x0c\r\x0e\x0f\x10\x11\x12\x13\x14'
    ),
}

AMBIGUOUS = {
    'DNA': ('N', '\x00'),
    'PROTEIN': ('XBZJUO', '\x00' * 6),
}


class SeqLoader(object):
    def __init__(self, alphabet='DNA'):
        self.alphabet, self.code = ALPHABETS[alphabet]
        alpha_ambi, code_ambi = AMBIGUOUS[alphabet]
        self.translator = maketrans(
            alpha_ambi + self.alphabet, code_ambi + self.code)
        self.alpha_nb = len(self.alphabet)

    def pad_seq(self, seq):
        seq = pad_sequences(seq, pre_padding=self.pre_padding,
                            maxlen=self.maxlen, padding='post',
                            truncating='post', dtype='int64')
        self.maxlen = seq.shape[1]
        return seq

    def seq2index(self, seq):
        seq = seq.translate(self.translator)
        seq = np.fromstring(seq, dtype='uint8')
        return seq.astype('int64')

    def get_ids(self, ids=None):
        pass

    def get_tensor(self, dataid, split='train', noise=0.0, fixed_noise=0.0,
                   val_split=0.25, top=False, generate_neg=True):
        df = self.load_data(dataid, split)
        if fixed_noise > 0.:
            df = augment(df, noise=fixed_noise, max_index=self.alpha_nb)
        if split == 'train' and generate_neg and hasattr(self, "aug_neg"):
            df = self.aug_neg(df)
        X, y = df['seq_index'], df['y'].values
        X = self.pad_seq(X)
        if top:
            X, _, y, _ = train_test_split(
                X, y, stratify=y, train_size=500)
        if split == 'train' and val_split > 0:
            X, X_val, y, y_val = train_test_split(
                X, y, test_size=val_split, stratify=y)
            X, y = torch.from_numpy(X), torch.from_numpy(y)
            X_val, y_val = torch.from_numpy(X_val), torch.from_numpy(y_val)
            train_dset = TensorDataset(
                X, y, noise=noise, max_index=self.alpha_nb)
            val_dset = TensorDataset(X_val, y_val, max_index=self.alpha_nb)
            return train_dset, val_dset
        X, y = torch.from_numpy(X), torch.from_numpy(y)
        return TensorDataset(X, y, noise=noise, max_index=self.alpha_nb)


class EncodeLoader(SeqLoader):
    def __init__(self, datadir, ext='.seq.gz', maxlen=None,
                 pre_padding=0):
        super(EncodeLoader, self).__init__()
        self.datadir = datadir
        self.ext = ext
        self.pre_padding = pre_padding
        self.maxlen = maxlen

    def get_ids(self, ids=None):
        targetnames = sorted(
            [filename.replace("_AC"+self.ext, "") for filename in os.listdir(
                self.datadir) if filename.endswith("_AC"+self.ext)])
        if ids is not None and ids != []:
            targetnames = [
                targetnames[tfid] for tfid in ids if tfid < len(targetnames)
            ]
        return targetnames

    def load_data(self, tfid, split='train'):
        if split == 'train':
            filename = "%s/%s_AC%s" % (self.datadir, tfid, self.ext)
        else:
            filename = "%s/%s_B%s" % (self.datadir, tfid, self.ext)
        df = pd.read_csv(filename, compression='gzip', delimiter='\t')
        df.rename(columns={'Bound': 'y', 'seq': 'Seq'}, inplace=True)
        df['seq_index'] = df['Seq'].apply(self.seq2index)
        return df

    def aug_neg(self, df):
        df2 = df.copy()
        df2['Seq'] = df['Seq'].apply(doublet_shuffle)
        df2['y'] = 0
        df2['EventID'] += '_neg'
        df2.index += df2.shape[0]
        df2['seq_index'] = df2['Seq'].apply(self.seq2index)
        df2 = pd.concat([df, df2])
        return df2


class SCOPLoader(SeqLoader):
    def __init__(self, datadir='data/SCOP', ext='fasta', maxlen=400,
                 pre_padding=0):
        super(SCOPLoader, self).__init__('PROTEIN')
        self.datadir = datadir
        self.ext = ext
        self.pre_padding = pre_padding
        self.maxlen = maxlen
        self.filename_tp = datadir + '/{}-{}.{}.' + ext

    def get_ids(self, ids=None):
        names = sorted([".".join(filename.split('.')[1:-1])
                       for filename in os.listdir(self.datadir)
                       if filename.startswith('pos-train')])
        if ids is not None and ids != []:
            names = [names[index] for index in ids if index < len(names)]
        return names

    def get_nb(self, dataid, split='train'):
        pos = self.filename_tp.format('pos', split, dataid)
        neg = self.filename_tp.format('neg', split, dataid)
        return self.get_nb_fasta(pos) + self.get_nb_fasta(neg)

    def load_data(self, dataid, split='train'):
        pos = self.filename_tp.format('pos', split, dataid)
        neg = self.filename_tp.format('neg', split, dataid)
        table = defaultdict(list)
        for y, filename in enumerate([neg, pos]):
            for record in SeqIO.parse(filename, self.ext):
                seq = str(record.seq).upper()
                name = record.id
                table['Seq'].append(seq)
                table['seq_index'].append(self.seq2index(seq))
                table['name'].append(name)
                table['y'].append(y)
        df = pd.DataFrame(table)
        return df


if __name__ == '__main__':
    load = EncodeLoader('../../data/encode')

