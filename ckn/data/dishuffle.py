import numpy as np
from collections import defaultdict
import sys
if sys.version_info < (3,):
    import string
    maketrans = string.maketrans
else:
    maketrans = str.maketrans

_RNA = maketrans('U', 'T')


def seq_to_graph(seq):
    graph = defaultdict(list)
    for i in range(len(seq) - 1):
        graph[seq[i]].append(seq[i+1])
    return graph


def graph_to_seq(graph, first_vertex, len_seq):
    is_done = False
    new_seq = first_vertex
    while not is_done:
        last_vertex = new_seq[-1]
        new_seq += graph[last_vertex][0]
        graph[last_vertex].pop(0)
        if len(new_seq) >= len_seq:
            is_done = True
    return new_seq


# function from: http://www.python.org/doc/essays/graphs/
def find_path(graph, start, end, path=[]):
    path = path + [start]
    if start == end:
        return path
    if start not in graph:
        return None
    for node in graph[start][-1]:
        if node not in path:
            newpath = find_path(graph, node, end, path)
            if newpath:
                return newpath
        return None


def sample_new_graph(graph, last_vertex):
    new_graph = defaultdict(list)
    for vertex in graph:
        edges = graph[vertex]
        np.random.shuffle(edges)
        new_graph[vertex].extend(edges)
    for vertex in new_graph:
        if not find_path(new_graph, vertex, last_vertex):
            return False, new_graph
    return True, new_graph


def doublet_shuffle(seq):
    seq = seq.upper().translate(_RNA)
    last_vertex = seq[-1]
    graph = seq_to_graph(seq)

    is_eulerian = False
    while not is_eulerian:
        is_eulerian, new_graph = sample_new_graph(graph, last_vertex)
    new_seq = graph_to_seq(new_graph, seq[0], len(seq))

    return new_seq
