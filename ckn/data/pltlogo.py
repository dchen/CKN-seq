import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.text import TextPath
from matplotlib.patches import PathPatch
from matplotlib.font_manager import FontProperties
import seaborn as sns
sns.set_style("white")
import numpy as np

DNA_ALPHABET = ['A', 'C', 'G', 'T']

DEEPBIND_PL = {'G': 'orange',
               'A': 'lime',
               'C': 'blue',
               'T': 'crimson'}

MEME_PL = {'G': 'orange',
           'A': 'red',
           'C': 'blue',
           'T': 'darkgreen'}

PALLETE = {'meme': MEME_PL, 'deepbind': DEEPBIND_PL}


def trim_pwm(info):
    max_ic = np.max(info)
    ic_threshold = np.max([0.1*max_ic, 0.1])
    w = info.shape[0]
    pfm_st = 0
    pfm_sp = w
    for i in range(w):
        if info[i] < ic_threshold:
            pfm_st += 1
        else:
            break
    for inf in reversed(info):
        if inf < ic_threshold:
            pfm_sp -= 1
        else:
            break
    return pfm_st, pfm_sp


def pwm_to_bits(pwm, trim=False):
    """pwm: m x 4 dataframe
    """
    pwm = np.asarray(pwm)
    info = np.asarray(np.log2(
        pwm.shape[1]) + np.sum(pwm * np.ma.log2(pwm), axis=1, keepdims=True))
    print("IC: {}".format(np.sum(info)))
    bits = pwm * info
    if trim:
        bits_st, bits_sp = trim_pwm(info)
        return bits[bits_st:bits_sp]
    return bits


def draw_logo(bits, width_scale=0.8, font_family="Arial", palette="meme"):
    try:
        alphabet_inverse = bits.columns[::-1]
    except:
        alphabet_inverse = DNA_ALPHABET[::-1]
    fp = FontProperties(family=font_family, weight="bold")

    COLOR_SCHEME = PALLETE[palette]

    def draw_base(letter, x, y, yscale=1, ax=None):
        text = TextPath((0, 0), letter, size=1, prop=fp)
        bb = text.get_extents()

        h, w = bb.height, bb.width
        anchor = bb.xmin

        w_scale = 1./w
        h_scale = yscale * 0.95 / h

        transform = mpl.transforms.Affine2D().scale(w_scale, h_scale) + \
            mpl.transforms.Affine2D().translate(x - 0.5 - w_scale*anchor, y) + \
            ax.transData
        txt = PathPatch(
            text, lw=0, fc=COLOR_SCHEME[letter],  transform=transform)
        if ax is not None:
            ax.add_artist(txt)
        return txt

    fig, ax = plt.subplots(figsize=(len(bits)*width_scale, 3))

    x = 1
    for i in range(len(bits)):
        scores = bits[i]
        y = 0
        for score, base in sorted(
                zip(scores[::-1], alphabet_inverse), key=lambda x: x[0]):
            draw_base(base, x, y, score, ax)
            y += score
        x += 1

    ax.set_yticks(range(0, 3))
    ax.set_xticks(range(1, x))
    sns.despine(ax=ax, trim=True)
    ax.set_xlim((0, x))
    ax.set_ylabel("bits", fontsize=15)
    try:
        fig.tight_layout()
    except:
        pass


if __name__ == '__main__':
    from Bio import motifs
    from Bio.Seq import Seq
    import pandas as pd
    instances = [Seq('AACTCACGAAT'), Seq('ACTAAACGAAA'), Seq('ACGACAATAAC')]
    m = motifs.create(instances)
    df = pd.DataFrame.from_dict(m.pwm)
    draw_logo(df, width_scale=0.8, palette='meme')
    plt.savefig("test.png", dpi=200)
